<?php
/**
 * Functions and definitions
 *
 * @package cpschool
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$cpschool_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/hooks-custom.php',                           // Custom hooks.
	'/plugins/advanced-custom-fields-setup.php',             // Load Editor functions.
	'/plugins/advanced-custom-fields/acf.php',             // Load Editor functions.
	'/plugins/kirki/kirki.php',             // Load Editor functions.
	'/plugins/menu-icons/wp-menu-icons.php',             // Load Editor functions.
	'/plugins/breadcrumbs.php',                  // Initialize theme default settings.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/hooks-wp.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/comments.php',                 // Custom Comments file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/editor.php',                          // Load Editor functions.
);

foreach ( $cpschool_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
