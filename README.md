# CampusPress Flex Theme

Website: [https://campuspress.com/](https://campuspress.com/)

## About

New, flexible theme from CampusPress for accessible, fast, and an easy-to-navigate site. Perfect for your new school, district, or department.

## License
CampusPress Flex WordPress Theme, Copyright 2013-2018 Holger CampusPress
CampusPress Flex is distributed under the terms of the GNU GPL version 2

http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

## Changelog
See [changelog](CHANGELOG.md)


## Basic Features

- 

## Developing With npm, Gulp and SASS and [Browser Sync][1]

### Installing Dependencies
- Make sure you have installed Node.js and Browser-Sync (optional) on your computer globally
- Then open your terminal and browse to the location of your UnderStrap copy
- Run: `$ npm install`

### Running
To work with and compile your Sass files on the fly start:

- `$ gulp watch`

Or, to run with Browser-Sync:

- First change the browser-sync options to reflect your environment in the file `/gulpconfig.json` in the beginning of the file:
```javascript
{
    "browserSyncOptions" : {
        "proxy": "localhost/theme_test/", // <----- CHANGE HERE
        "notify": false
    },
    ...
};
```
- then run: `$ gulp watch-bs`

## Footnotes

[1] Visit [http://browsersync.io](http://browsersync.io) for more information on Browser Sync

### Licenses & Credits
=
- Underscores: http://underscores.me/ | GNU GPL 
- UnderStrap: http://understrap.com | GNU GPL
- Bootstrap: http://getbootstrap.com | https://github.com/twbs/bootstrap/blob/master/LICENSE (Code licensed under MIT documentation under CC BY 3.0.)
- jQuery: https://jquery.org | (Code licensed under MIT)
- WP Bootstrap Navwalker by Edward McIntyre: https://github.com/twittem/wp-bootstrap-navwalker | GNU GPL
- TwentyTwenty: https://github.com/WordPress/twentytwenty | GNU GPL
- Kirki: https://kirki.org/ | MIT
- WP Menu Icons: https://wordpress.org/plugins/wp-menu-icons/ | GNU GPL