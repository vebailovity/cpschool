<?php 
$hero_style = cpschool_get_hero_style();

if( in_array($hero_style, array(false, 'disabled', 'full-title-under-img')) || is_customize_preview() ) { ?>
    <?php
    $page_title = cpschool_get_page_title();

    if($page_title) {
        $page_subtitle = cpschool_get_page_subtitle();
    ?>
        <header <?php cpschool_class('page-header', 'page-header'); ?>>
            <?php
            if( cpschool_is_breadcrumb_enabled( 'page' ) || is_customize_preview() ) { 
                cpschool_show_breadcrumb('page-breadcrumb'); 
            }
            echo '<h1 class="page-title">'.cpschool_get_page_title().'</h1>';
            if($page_subtitle) {
                echo '<div class="page-meta entry-meta">'.cpschool_get_page_subtitle().'</div>';
            }
            ?>
        </header><!-- .page-header -->
    <?php
    }
    ?>
<?php 
} 
?>