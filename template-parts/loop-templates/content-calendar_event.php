<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package cpschool
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$hero_style = cpschool_get_hero_style();

if(is_single()) {
	$classes_prefix = 'entry-single';
}
elseif(is_singular()) {
	$classes_prefix = 'page';
}
else {
	$classes_prefix = 'entry';
}

$title_hero_disabled = get_post_meta(get_the_ID(), 'cps_hero_title_disable', true);
?>

<?php if( !is_singular() ) { ?>
	<div <?php cpschool_class('entry-col', 'entry-col'); ?>>
<?php } ?>
		<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

			<?php if( !is_singular() || ( (!$title_hero_disabled && in_array($hero_style, array(false, 'disabled', 'full-title-under-img'))) || is_customize_preview() )) { ?>	
				<?php if(is_singular()) { ?>
					<header <?php cpschool_class('page-header', 'page-header'); ?>>
						<?php
						if( cpschool_is_breadcrumb_enabled( 'page' ) || is_customize_preview() ) { 
							cpschool_show_breadcrumb('page-breadcrumb'); 
						}
						the_title('<h1 class="page-title entry-title">','</h1>');
						?>
				<?php } else { ?>
					<header <?php cpschool_class('entry-header', 'entry-header'); ?>>
						<?php
						the_title(
							sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
							'</a></h2>'
						);
						?>
				<?php } ?>

						<?php if ( 'post' == get_post_type() ) : ?>
							<div class="<?php if(is_singular()) { 'page-meta '; }; ?>entry-meta">
								<?php echo cpschool_get_post_meta(get_the_ID()); ?>
							</div><!-- .entry-meta -->
						<?php endif; ?>

					</header><!-- .entry-header -->
			<?php } ?>

			<?php 
			if( has_post_thumbnail() ) {
					if( ( get_theme_mod('entries_lists_featured_image_style') != 'disabled' && ( is_archive() || is_home() ) ) || is_customize_preview() ) {
			?>
				<div <?php cpschool_class($classes_prefix.'-featured-image', 'entry-featured-image'); ?>>
					<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
				</div>
			<?php 
				}
			} 
			?>

			<div class="entry-content">
				<div class="event-meta mb-3">
					<?php if ( ! empty( calendarp_event_human_read_dates( 'date' ) ) ) : ?>
						<div class="event-meta-item event-dates"><span class="dashicons dashicons-calendar-alt"></span> <?php echo calendarp_event_human_read_dates( 'date' ); ?></div>
					<?php endif; ?>

					<?php if ( ! empty( calendarp_event_human_read_dates( 'recurrence' ) ) ) : ?>
						<div class="event-meta-item event-recurrence"><span class="dashicons dashicons-update"></span> <?php echo calendarp_event_human_read_dates( 'recurrence' ); ?></div>
					<?php endif; ?>

					<?php if ( ! empty( calendarp_event_human_read_dates( 'time' ) ) ) : ?>
						<div class="event-meta-item event-time"><span class="dashicons dashicons dashicons-clock"></span> <?php echo calendarp_event_human_read_dates( 'time' ); ?></div>
					<?php endif; ?>

					<div class="event-meta-item event-categories">
						<?php calendarp_event_categories_list(); ?>
					</div>

					<div class="event-meta-item">
						<?php _e( 'Add to', 'calendar-plus' ); ?>:
						<?php calendarp_event_add_to_calendars_links(); ?>
					</div>
				</div>

				<?php if ( is_single() && calendarp_event_has_location() ) : ?>
					<div class="event-location mb-3">
						<p><span class="dashicons dashicons-location"></span> <?php echo calendarp_the_event_location()->get_full_address(); ?></p>
						<div class="event-location-description"><?php echo calendarp_get_location_description(); ?></div>
						<?php echo calendarp_get_google_map_html( calendarp_the_event_location()->ID ); ?>
					</div>
				<?php endif; ?>

				<?php do_action( 'calendarp_content_event_content', calendarp_the_event() ); ?>

				<?php
				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'cpschool' ),
						'after'  => '</div>',
					)
				);
				?>

			</div><!-- .entry-content -->

			<footer class="entry-footer">

				<?php //cpschool_entry_footer(); ?>

			</footer><!-- .entry-footer -->

		</article><!-- #post-## -->
<?php if( !is_singular() ) { ?>
	</div>
<?php } ?>