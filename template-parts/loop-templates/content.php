<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package cpschool
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$hero_style = cpschool_get_hero_style();

if(is_single()) {
	$classes_prefix = 'entry-single';
}
elseif(is_singular()) {
	$classes_prefix = 'page';
}
else {
	$classes_prefix = 'entry';
}

$title_hero_disabled = get_post_meta(get_the_ID(), 'cps_hero_title_disable', true);
?>

<?php if( !is_singular() ) { ?>
	<div <?php cpschool_class('entry-col', 'entry-col'); ?>>
<?php } ?>
		<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

			<?php if( !is_singular() || ( (!$title_hero_disabled && in_array($hero_style, array(false, 'disabled', 'full-title-under-img'))) || is_customize_preview() )) { ?>	
				<?php if(is_singular()) { ?>
					<header <?php cpschool_class('page-header', 'page-header'); ?>>
						<?php
						if( cpschool_is_breadcrumb_enabled( 'page' ) || is_customize_preview() ) { 
							cpschool_show_breadcrumb('page-breadcrumb'); 
						}
						the_title('<h1 class="page-title entry-title">','</h1>');
						?>
				<?php } else { ?>
					<header <?php cpschool_class('entry-header', 'entry-header'); ?>>
						<?php
						the_title(
							sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
							'</a></h2>'
						);
						?>
				<?php } ?>

						<?php if ( 'post' == get_post_type() ) : ?>
							<div class="<?php if(is_singular()) { 'page-meta '; }; ?>entry-meta">
								<?php echo cpschool_get_post_meta(get_the_ID()); ?>
							</div><!-- .entry-meta -->
						<?php endif; ?>

					</header><!-- .entry-header -->
			<?php } ?>

			<?php 
			if( has_post_thumbnail() ) {
					if( ( get_theme_mod('entries_lists_featured_image_style') != 'disabled' && ( is_archive() || is_home() ) ) || is_customize_preview() ) {
			?>
				<div <?php cpschool_class($classes_prefix.'-featured-image', 'entry-featured-image'); ?>>
					<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
				</div>
			<?php 
				}
			} 
			?>

			<div class="entry-content">
				<?php if(is_singular() || (get_theme_mod('entries_lists_content_type', 'excerpt') == 'content')) { ?>
					<?php the_content(); ?>
				<?php } else { ?>
					<?php the_excerpt(); ?>
				<?php } ?>

				<?php
				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'cpschool' ),
						'after'  => '</div>',
					)
				);
				?>

			</div><!-- .entry-content -->

			<footer class="entry-footer">

				<?php //cpschool_entry_footer(); ?>

			</footer><!-- .entry-footer -->

		</article><!-- #post-## -->
<?php if( !is_singular() ) { ?>
	</div>
<?php } ?>