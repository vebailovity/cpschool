<?php

if (!defined('ABSPATH')) {
  die('-1');
}

if (!class_exists('WPMI_Frontend')) {

  class WPMI_Frontend extends WPMI {

    private static $instance;

    function nav_menu_item_title($title, $menu_item_id) {

      $classes = array();

      $wpmi = $style = $size = $color = '';

      $new_title = $title;

      if (!is_admin() && !wp_doing_ajax()) {

        if ($wpmi = get_post_meta($menu_item_id, WPMI_DB_KEY, true)) {

          if (isset($wpmi['icon']) && $wpmi['icon'] != '') {

            foreach ($wpmi as $key => $value) {

              if (in_array($key, array('position')) && $value != '') {
                $classes[] = "nav-icon-{$key}-{$value}";
              }

              if ($key === 'icon' && $value) {
                //EB-TODO this should not be done here
                if (strpos($value, 'dashicon') !== false) {
                  wp_enqueue_style( 'dashicons' );
                }
                $classes[] = str_replace('dashicons ', 'cps-icon cps-dashicon ', $value);
              }
            }

            if (!empty($wpmi['label'])) {
              $title = '<span class="screen-reader-text">'.$title.'</span>';
              $classes[] = 'nav-icon-no-label';
            }

            $color_val      = ( isset($wpmi['color']) ) ? $wpmi['color'] : '';
            $bgcolor_val    = ( isset($wpmi['bgcolor']) ) ? $wpmi['bgcolor'] : '';
            
            $color   = $this->get_hex_from_color_val( $color_val );
            $bgcolor = $this->get_hex_from_color_val( $bgcolor_val );

            $styles = array();
            if (!empty($color)) {
              $styles[] = 'color:' . $color;
            }

            if (!empty($bgcolor)) {
              $styles[] = 'background-color:' . $bgcolor;
              $classes[] = 'nav-icon-has-bg';
            }

            if($styles) {
              $style = ' style="' . esc_attr( implode(';', $styles) .';' ) . '"';
            }

            $icon = '<i' . $style . ' class="nav-icon ' . join(' ', array_map('esc_attr', $classes)) . '"></i>';

            if (isset($wpmi['position']) && $wpmi['position'] == 'after') {
              $new_title = $title . $icon;
            } else {
              $new_title = $icon . $title;
            }
          }
        }
      }

      return apply_filters('wp_menu_icons_item_title', $new_title, $menu_item_id, $wpmi, $title);
    }

    function init() {
      add_filter('the_title', array($this, 'nav_menu_item_title'), 999, 2);
    }

    public static function instance() {
      if (!isset(self::$instance)) {
        self::$instance = new self();
        self::$instance->init();
      }
      return self::$instance;
    }

  }

  WPMI_Frontend::instance();
}

