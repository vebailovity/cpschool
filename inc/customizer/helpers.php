<?php
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'cpschool_get_customizer_typography_choices' ) ) {
	// Generates array with options used with typography control so all font lists have the same choises.
	function cpschool_get_customizer_typography_choices() {
		return array( 
			'fonts' => [
				'google'   => [ 'popularity', 10 ],
				'standard' => [
					'Arial, Helvetica, sans-serif',
					'"Lucida Sans Unicode", "Lucida Grande", sans-serif',
					'Tahoma, Geneva, sans-serif',
					'Verdana, Geneva, sans-serif',
					'Georgia, serif',
					'"Palatino Linotype", "Book Antiqua", Palatino, serif',
					'"Times New Roman", Times, serif',
					'"Courier New", Courier, monospace',
					'"Lucida Console", Monaco, monospace',
				],
			],
		);
	}
}

if ( ! function_exists( 'cpschool_get_customizer_fonts_options' ) ) {
	// Generates array with options used with typography control so all font lists have the same choises.
	function cpschool_get_customizer_fonts_options($context = false, $default = false) {
		if($default) {
			$default = array('inherit' => esc_html__( 'Default font', 'cpschool' ));
		}
		else {
			$default = array();
		}
		if($context == 'header') {
			return $default + array( 
				'public_sans' => 'Public Sans',
				'amstelvar' => 'Amstelvar',
				'commissioner' => 'Commissioner',
				'epilogue' => 'Epilogue',
				'gelasio' => 'Gelasio',
				'hepta_slab' => 'Hepta Slab',
				'inter' => 'Inter',
				'lexend' => 'Lexend',
				'manrope' => 'Manrope',
				'merriweather' => 'Merriweather',
				'mohave' => 'Mohave',
				'petrona' => 'Petrona',
				'russolo' => 'Russolo',
				'space_grotesk' => 'Space Grotesk',
				//'urbanist' => 'Urbanist',
			);
		}
		else {
			return $default + array( 
				'public_sans' => 'Public Sans',
				'amstelvar' => 'Amstelvar',
				'commissioner' => 'Commissioner',
				//'epilogue' => 'Epilogue',
				//'gelasio' => 'Gelasio',
				'hepta_slab' => 'Hepta Slab',
				'inter' => 'Inter',
				'lexend' => 'Lexend',
				'manrope' => 'Manrope',
				'merriweather' => 'Merriweather',
				//'mohave' => 'Mohave',
				'petrona' => 'Petrona',
				//'russolo' => 'Russolo',
				//'space_grotesk' => 'Space Grotesk',
				//'urbanist' => 'Urbanist',
			);
		}
	}
}

if ( ! function_exists( 'cpschool_generate_customizer_color_settings' ) ) {
	// Generates customizer options used to generate additional colors from source colors.
	function cpschool_generate_customizer_color_settings($prefix) {
		$settings_to_generate = array('contrast', 'accent', 'accent-a', 'accent-contrast', 'accent-hl', 'accent-hl-a', 'accent-hl-contrast');
		foreach($settings_to_generate as $setting) {
			$name = $prefix.'-'.$setting;
			$css_name = '--'.$name;
			$setting_name = str_replace('-', '_', $name);

			Kirki::add_field( 'cpschool', [
				'type'        => 'hidden',
				'settings'    => $setting_name,
				'section'     => 'colors',
				'transport'   => 'auto',
				'output' => array( 
					array(
						'element'  => ':root',
						'property' => $css_name,
						'context' => array('editor', 'front')
					),
				)
			] );
		}
	}
}

// Generate customizer options that are common for all sections under "Content" panel.
if ( ! function_exists( 'cpschool_generate_content_common_settings' ) ) {
	function cpschool_generate_content_common_settings($section) {
        //Lets set some default values
        $default = array();
        $default_map = array(
            'pages' => array('sidebar-left'),
            'posts' => array('sidebar-right'),
            'entries_lists' => array('sidebar-right')
        );
        if( isset($default_map[$section] ) ) {
            $default = $default_map[$section];
        }
        Kirki::add_field( 'cpschool', [
            'type'        => 'multicheck',
            'settings'    => $section.'_sidebars',
            'label'       => esc_html__( 'Sidebars', 'kirki' ),
            'description' => esc_html__( 'Choose which sidebars should be displayed on this page type. Empty sidebars won\'t be visible even when enabled.', 'cpschool' ),
            'section'     => $section,
            'default' =>  $default,
            'choices'     => [
                'sidebar-left' => esc_html__( 'Left Sidebar', 'kirki' ),
                'sidebar-right' => esc_html__( 'Right Sidebar', 'kirki' ),
            ],
		] );
    }
}