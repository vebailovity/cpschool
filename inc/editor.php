<?php
/**
 * Adjust editor
 *
 * @package cpschool
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Registers blocks stylesheet for the theme.
 */

add_action( 'enqueue_block_editor_assets', 'cpschool_add_editor_styles' );

if ( ! function_exists( 'cpschool_add_editor_styles' ) ) {
	function cpschool_add_editor_styles() {
		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/custom-editor-style.min.css' );
		wp_enqueue_style( 'cpschool-gutenberg', get_theme_file_uri( 'css/custom-editor-style.min.css' ), false, $css_version );
	}
}